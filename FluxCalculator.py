#  Script file to calculate flux emitted by a light source as specified by its config file

#  Import necessary modules
import numpy as np
import FluxCalculatorFunctions as fc
from scipy.interpolate import CubicSpline
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#  Set name of file to read
# filename = 'XZFBB53W-8'
# filename = 'XZFBB53W-8ST'
# filename = 'XZFBB54W-8'
# filename = 'APG1608QBC-D'
# filename = 'APT1608QBC-G'
# filename = 'APHCM2012QBC-D-F01'
#filename = 'APTD1608VBC-D'
# filename = 'datasheet-APTD1608VBC-D'
# filename = 'APHHS1005LQBC-D-V'
# filename = 'XPE2'
filename = 'XZBBR'


filetype = '.txt'
directory_string = "C:\\Users\\amoo496\\PycharmProjects\\Power_meter\\ConfigFiles\\"

#  Read data from config file
config_data = np.loadtxt(directory_string + filename + filetype, dtype=float)
luminous_intensity = config_data[0] # mcd
forward_current = config_data[1]  # mA
emitting_area = config_data[2]  # mm^2
wavelength = config_data[3]  # nm
n_spline_points = int(config_data[4])
theta_spline = config_data[5:5 + n_spline_points] * np.pi/180  # convert to radians
intensity_spline = config_data[5 + n_spline_points:5 + 2 * n_spline_points]

#  read CIE 1964 colour reference data
colour_data = np.loadtxt("C:\\Users\\amoo496\\PycharmProjects\\Power_meter\\1964_colour_reference.txt")

#  Calculate radiant intensity of LED
if filename == 'XPE2':
    radiant_intensity = luminous_intensity
else:
    radiant_intensity = fc.luminous_to_radiant_intensity(luminous_intensity / 1000, wavelength, colour_data) * 1000  # mW/Sr

#  Create cubic spline of radiation diagram
relative_intensity_spline = CubicSpline(theta_spline, intensity_spline)

#  choose number of steps to discretise problem in to
n_steps = 10000

#  choose maximum angle from azimuth to sum flux within
angle = 90

#  determine size of each step (around hemisphere) and allocate space for theta values and solid angle of each step
step_size = angle * np.pi/(180 * (n_steps - 1))
theta_values = []
solid_angle = np.zeros(n_steps)

theta_step = 0
for step in range(n_steps):
    theta_next = theta_step + step_size
    theta_values.append(theta_step)

    if step == (n_steps - 1):
        theta_values[-1] = theta_values[-2] + step_size
    else:
        solid_angle[step+1] = 2 * np.pi * (np.cos(theta_step) - np.cos(theta_next))

    theta_step = theta_next

# allocate memory for flux calculation
point_intensities = np.zeros([n_steps])
flux = np.zeros([n_steps])

#  calculate total flux for each guesses peak intensity
for step in range(n_steps):
    point_intensities[step] = relative_intensity_spline(theta_values[step]) * radiant_intensity
    if step == 0:
        pass
    else:
        flux[step] = solid_angle[step] * (point_intensities[step] + point_intensities[step - 1]) / 2
flux_total = sum(flux)

irradiance = flux_total / emitting_area

#  Plot radiation profile and save as .png
fc.plot_radiation_profile(relative_intensity_spline, filename + '_RadiationProfile')

#  write results and script parameters to a .txt file (n_steps, angle, radiant_intensity
file = open(filename + "_results" + filetype, "w")
# file.write(forward_current, angle, wavelength, luminous_intensity, radiant_intensity, flux_total)
file.write("mA\tdeg\tnm\tmcd\tmW/Sr\tmm^2\tmW\tmW/mm^2\n")
file.write("{0:0.2f}\t{1:2.2f}\t{2:3.2f}\t{3:3.2f}\t{4:3.2f}\t{5:1.4f}\t{6:1.3f}\t{7:3.3f}".format(forward_current, angle, wavelength, luminous_intensity, radiant_intensity, emitting_area, flux_total, irradiance))
file.close()

