from matplotlib import pyplot as plt
import numpy as np


def luminous_to_radiant_intensity(luminous_intensity, wavelength, colour_data):
    """function takes luminous intensity (cd) and wavelength of light as arguments and converts to radiant intensity"""

    if (wavelength < 360) | (wavelength > 559):
        raise Exception("Input error: wavelength outside of colour-reference data range")

    if (wavelength % 1) != 0:  # wavelength is not an integer
        distance = wavelength - int(wavelength)
        row_index = int(wavelength) - 360
        luminosity = linear_interp(distance, colour_data[row_index][1], colour_data[row_index + 1][1])
    else:  # wavelength is an integer
        luminosity = colour_data[int(wavelength) - 360][1]

    radiant_intensity = luminous_intensity / (683 * luminosity)
    #radiant_intensity = 0.002
    return radiant_intensity


def linear_interp(distance, lower_bound, upper_bound):
    """function takes a value and linearly interpolates it between the specified bounds"""
    # distance = (input_value - lower_bound) / (upper_bound - lower_bound)
    interpolated_value = lower_bound * (1 - distance) + upper_bound * distance
    return interpolated_value


def plot_radiation_profile(relative_intensity_spline, main_title):
    """Plots radiation profile contained in the input spline in Cartesian and Polar coordinates from 0 to pi/2"""
    # Generate data to plot from spline
    n_steps = 10000
    plot_step_size = 0.5 * np.pi / (n_steps - 1)
    theta = np.zeros(n_steps)
    intensity = np.zeros(n_steps)
    for step in range(n_steps):
        theta[step] = step * plot_step_size
        intensity[step] = relative_intensity_spline(theta[step])

    # Generate plot
    fig1 = plt.figure()
    ax1 = fig1.add_axes([0.1, 0.1, 0.8, 0.8], polar=True)
    ax1.set_theta_direction(-1)
    ax1.set_theta_offset(0.5 * np.pi)

    ax1.set_ylim(0, 1)
    ax1.set_yticks(np.arange(0, 1.1, 0.1))
    ax1.set_xlim(0, 0.5 * np.pi)

    ax1.plot(theta, intensity)
    ax1.set_title(main_title)
    plt.savefig(main_title + '.png', bbox_inches='tight')
    plt.show()

    return
